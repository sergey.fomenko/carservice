package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.garage.carservice.backend.model.customer.CarEntity;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, Integer> {
    boolean existsByVinCodeOrGovNumber(String vinCode, String govNumber);
}
