package com.garage.carservice.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EntityScan(basePackages = {"com.garage.carservice.backend"})
public class RSocketServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RSocketServiceApplication.class, args);
	}

}
