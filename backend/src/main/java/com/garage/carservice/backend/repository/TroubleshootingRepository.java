package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.TroubleshootingEntity;

public interface TroubleshootingRepository extends JpaRepository<TroubleshootingEntity, Integer> {
}
