package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.customer.ClientEntity;

public interface ClientRepository extends JpaRepository<ClientEntity, Integer> {
    ClientEntity findByPhonesValue(String value);
}
