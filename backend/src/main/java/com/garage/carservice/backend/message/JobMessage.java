package com.garage.carservice.backend.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalTime;

@Getter
@Setter
@ToString
@SuperBuilder
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobMessage  implements Serializable {

    Integer id;
    String value;
    LocalTime startTime;
    LocalTime durationTime;
    String workerPhoto;
    CarMessage car;
}
