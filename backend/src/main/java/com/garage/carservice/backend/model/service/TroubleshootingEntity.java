package com.garage.carservice.backend.model.service;

import com.garage.carservice.backend.model.AuditableEntity;
import com.garage.carservice.backend.model.customer.CarEntity;
import com.garage.carservice.backend.model.warehouse.DefectEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "troubleshooting")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class TroubleshootingEntity extends AuditableEntity {

    @Column
    String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "car_id")
    CarEntity car;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "executor_id")
    ExecutorEntity executor;

    @Column(name = "date")
    LocalDate date;

    @ManyToMany
    @JoinTable(name = "troubleshooting_defects",
            joinColumns = {@JoinColumn(name = "troubleshooting_id")},
            inverseJoinColumns = {@JoinColumn(name = "defect_id")})
    @EqualsAndHashCode.Exclude
    private Set<DefectEntity> defects = new HashSet<>();

}
