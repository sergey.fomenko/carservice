package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.warehouse.PartEntity;

public interface PartRepository extends JpaRepository<PartEntity, Integer> {
}
