package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.customer.ClientPaymentsEntity;

public interface ClientPayRepository extends JpaRepository<ClientPaymentsEntity, Integer> {
}
