package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.BoxPlaceEntity;

public interface BoxPlaceRepository extends JpaRepository<BoxPlaceEntity, Integer> {
}
