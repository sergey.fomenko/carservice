package com.garage.carservice.backend.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@SuperBuilder
@FieldDefaults(level = AccessLevel.PROTECTED)
public class AuditableEntity extends BaseEntity {

	@Column(name = "updated_time")
	LocalDateTime updatedTime;

	@Override
	@PrePersist
	void onInsert() {
		this.createdTime = LocalDateTime.now();
		this.updatedTime = this.createdTime;
	}

	@PreUpdate
	void onUpdate() {
		this.updatedTime = LocalDateTime.now();
	}
}
