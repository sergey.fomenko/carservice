package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.customer.BankAccountEntity;

public interface BankAccountRepository extends JpaRepository<BankAccountEntity, Integer> {
    boolean existsByValue(String value);
}
