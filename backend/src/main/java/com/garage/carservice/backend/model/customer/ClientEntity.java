package com.garage.carservice.backend.model.customer;

import com.garage.carservice.backend.model.AuditableEntity;
import com.garage.carservice.backend.model.UserEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "clients")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
@ToString()
public class ClientEntity extends AuditableEntity {

        @Column
        String fio;

        @Column
        String description;

        @Column
        LocalDate birthday;

        @Column
        String sex;

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        UserEntity user;

        @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        List<CarEntity> cars;

        @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
        List<PhoneEntity> phones;

        @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
        List<BankAccountEntity> bankCards;
}
