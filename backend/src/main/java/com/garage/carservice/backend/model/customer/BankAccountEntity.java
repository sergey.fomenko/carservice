package com.garage.carservice.backend.model.customer;

import com.garage.carservice.backend.model.AuditableEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "bank_accounts")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class BankAccountEntity extends AuditableEntity {

        @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
        @JoinColumn(name = "client_id", referencedColumnName = "id", nullable = false)
        ClientEntity client;

        @Column(name = "client_id", updatable = false, insertable = false)
        private Integer clientId;

        @Column(name = "value")
        String value;
}
