package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.ServiceEntity;

public interface ServiceRepository extends JpaRepository<ServiceEntity, Integer> {
}
