package com.garage.carservice.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode
@Table(name="roles")
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserRole extends BaseEntity implements Serializable {

	@Enumerated(EnumType.STRING)
	@Column(name = "role")
	private UserRoles role;
}
