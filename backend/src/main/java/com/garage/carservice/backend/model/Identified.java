package com.garage.carservice.backend.model;

public interface Identified {
//    Integer getId();
//
//    void setId(Integer id);

    Integer getId();

    void setId(Integer id);
}
