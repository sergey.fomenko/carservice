package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.TaskTypeEntity;

public interface TaskTypeRepository extends JpaRepository<TaskTypeEntity, Integer> {
    boolean existsByType(String type);
}
