//package com.garage.carservice.backend.controller;
//
//import com.garage.carservice.backend.message.CarMessage;
//import com.garage.carservice.backend.message.JobMessage;
//import com.garage.carservice.backend.message.Message;
//import com.garage.carservice.backend.message.WorkPlaceMessage;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.Payload;
//import org.springframework.stereotype.Controller;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
//import java.time.Duration;
//import java.time.LocalTime;
//import java.util.List;
//
//@Slf4j
//@Controller
//public class WorkPlaceController {
//
//    @MessageMapping("workPlaces")
//    public Mono<List<WorkPlaceMessage>> requestResponse(final String request) {
//        log.info("Received request-response request: {}", request);
//        WorkPlaceMessage workPlace = getWorkPlaceMessage(1, "Сергей Пежо 407", LocalTime.of(8, 00), LocalTime.of(4, 15));
//        List list = List.of(workPlace);
//        return Mono.just(list);
//    }
//
//    private WorkPlaceMessage getWorkPlaceMessage(long index, String name, LocalTime start, LocalTime duration) {
//        CarMessage car = CarMessage.builder()
//                .id(1)
//                .value(name)
//                .carLogo("/logo.png")
//                .build();
//        JobMessage job = JobMessage.builder()
//                .id(1)
//                .value("TO 15000")
//                .startTime(start)
//                .durationTime(duration)
//                .car(car)
//                .workerPhoto("/photo.png")
//                .build();
//        WorkPlaceMessage workPlace = WorkPlaceMessage.builder()
//                .id(1)
//                .value("Подъемник 1")
//                .jobs(List.of(job))
//                .index(index)
//                .build();
//        return workPlace;
//    }
//
//    @MessageMapping("fire-and-forget")
//    public void fireAndForget(final Payload request) {
//        log.info("---------- Received fire-and-forget request: {}", request);
//    }
//
//    @MessageMapping("workPlacesStream")
//    public Flux<List<WorkPlaceMessage>> stream(final String request) {
//        log.info("Received stream request: {}", request);
//        return Flux
//                .interval(Duration.ofSeconds(20))//.onBackpressureBuffer()
//                .map(index -> List.of(getWorkPlaceMessage(index, "Сергей Пежо 407", LocalTime.of(8, 00), LocalTime.of(4, 15))))
//                .log();
//    }
//
//    @MessageMapping("channel")
//    public Flux<Message> channel(final Flux<Message> settings) {
//        log.info("Received channel request");
//        settings.subscribe(message -> log.info(message.toString()));
//
//        return Flux.interval(Duration.ofSeconds(1))
//                .doOnCancel(() -> log.warn("The client cancelled the channel."))
//                .map(index -> new Message("server", "stream", index));
//    }
//}
