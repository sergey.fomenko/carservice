package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.ExecutorEntity;

public interface ExecutorRepository extends JpaRepository<ExecutorEntity, Integer> {
}
