package com.garage.carservice.backend.model.service;

import com.garage.carservice.backend.model.AuditableEntity;
import com.garage.carservice.backend.model.customer.CarEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tech_services")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class TechServiceEntity extends AuditableEntity {

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "car_id")
    CarEntity car;

    @Column
    Integer mileage;

    @Column(name = "last_service_date")
    LocalDate lastServiceDate;

    @Column
    Boolean reminder;
}
