package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.TechServiceEntity;

public interface TechServiceRepository extends JpaRepository<TechServiceEntity, Integer> {
}
