package com.garage.carservice.backend.repository;

import com.garage.carservice.backend.model.service.SchedulerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchedulerRepository extends JpaRepository<SchedulerEntity, Integer> {
}
