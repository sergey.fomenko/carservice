//package com.garage.carservice.backend.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.ReactiveAuthenticationManager;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.stereotype.Component;
//import reactor.core.publisher.Mono;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Slf4j
//@Component
//public class RSocketAuthManager implements ReactiveAuthenticationManager {
//
//    @Override
//    public Mono<Authentication> authenticate(Authentication token) {
//        log.info("authenticate called with key: {}",  token.getCredentials().toString());
//        List<GrantedAuthority> authorities = new ArrayList();
//        return Mono.just(new UsernamePasswordAuthenticationToken("user", null, authorities));
//    }
//}
