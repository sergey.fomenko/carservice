package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.MaintenanceStatusEntity;

public interface MaintenanceStatusRepository extends JpaRepository<MaintenanceStatusEntity, Integer> {
}
