package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.customer.PhoneEntity;

public interface PhoneRepository extends JpaRepository<PhoneEntity, Integer> {
    boolean existsByValue(String value);
}
