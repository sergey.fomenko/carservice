package com.garage.carservice.backend.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.garage.carservice.backend.model.AuditableEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import com.garage.carservice.backend.model.service.ServiceEntity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cars")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class CarEntity extends AuditableEntity {

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id", nullable = false)
    ClientEntity owner;

    @Column(name = "client_id", updatable = false, insertable = false)
    @JsonIgnore
    private Integer ownerId;

    @Column
    String make;

    @Column
    String model;

    @Column(name = "vin_code", unique = true)
    @Pattern(regexp = "^[A-HJ-NPR-Za-hj-npr-z\\d]{12}\\d{5}$")
    String vinCode;

    @Column(name = "gov_number", unique = true)
    String govNumber;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "color_id")
    CarColorEntity color;

    @Column
    LocalDate year;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    List<ServiceEntity> services;
}
