package com.garage.carservice.backend.model;

import lombok.Getter;

@Getter
public enum UserRoles {

	ADMIN(1),
	MANAGER(2),
	CLIENT(3),
	EXECUTOR(4),
	PURCHASER(5),
	STOREKEEPER(6);

	private final Integer id;

	UserRoles(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return name();
	}
}
