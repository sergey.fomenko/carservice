package com.garage.carservice.backend.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@SuperBuilder
@FieldDefaults(level = AccessLevel.PROTECTED)
public class BaseEntity implements Identified, Serializable {

//	@Id
//	@GeneratedValue(generator = "uuid")
//	@GenericGenerator(name = "uuid", strategy = "uuid2")
//	@Column(name = "id", updatable = false, nullable = false)
//	@Type(type="pg-uuid")
//	Integer id;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer id;

	@Column(name = "created_time", updatable = false)
	LocalDateTime createdTime;

	@SuppressWarnings("unused")
	@PrePersist
	void onInsert() {
		this.createdTime = LocalDateTime.now();
	}

}
