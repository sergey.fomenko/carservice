package com.garage.carservice.backend.config;

import com.garage.carservice.backend.model.customer.BankAccountEntity;
import com.garage.carservice.backend.model.customer.CarColorEntity;
import com.garage.carservice.backend.model.customer.CarEntity;
import com.garage.carservice.backend.model.customer.ClientEntity;
import com.garage.carservice.backend.model.customer.PhoneEntity;
import com.garage.carservice.backend.repository.BankAccountRepository;
import com.garage.carservice.backend.repository.CarColorRepository;
import com.garage.carservice.backend.repository.CarRepository;
import com.garage.carservice.backend.repository.ClientRepository;
import com.garage.carservice.backend.repository.PhoneRepository;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Slf4j
@Component
@Setter(onMethod = @__({@Autowired}))
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DataInit  implements InitializingBean {

    ClientRepository clientRepository;
    BankAccountRepository bankAccountRepository;
    PhoneRepository phoneRepository;
    CarRepository carRepository;
    CarColorRepository carColorRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
//        postConstruct();
    }

    @Transactional
    void postConstruct() {
        log.info("---------------------------------------- Data Init ----------------------------------------");
        String phone = "380638271013";
        if (phoneRepository.existsByValue(phone)) {
            ClientEntity client = clientRepository.findByPhonesValue(phone);
            client.setDescription("Сергей Пежо407");
            client.setFio("Сергей Фоменко");
            ClientEntity saved = clientRepository.save(client);
            createCars(saved);

        } else {
            ClientEntity client = ClientEntity.builder()
                    .fio("Сергей Фоменко")
                    .description("Сергей Пежо407")
                    .sex("M")
                    .birthday(LocalDate.now())
                    .build();
            ClientEntity saved = clientRepository.save(client);
            createBankCards(saved);
            createPhones(saved, phone);
            createCars(saved);
        }

    }

    void createBankCards(ClientEntity client) {
        String value = "5331948212186448";
        if (bankAccountRepository.existsByValue(value)) {
            return;
        } else {
            BankAccountEntity bankAccountEntity = BankAccountEntity.builder()
                    .value(value)
                    .client(client)
                    .clientId(client.getId())
                    .build();
            bankAccountRepository.save(bankAccountEntity);
        }
    }

    void createPhones(ClientEntity client, String value) {
        if (phoneRepository.existsByValue(value)) {
            return;
        } else {
            PhoneEntity phoneEntity = PhoneEntity.builder()
                    .value(value)
                    .client(client)
                    .clientId(client.getId())
                    .build();
            phoneRepository.save(phoneEntity);
        }
    }

    void createCars(ClientEntity client) {
        String govNumber1 = "AE9977CP";
        String vinCode1 = "JKBZR800CDDA10053";
        CarColorEntity color1 = carColorRepository.save(CarColorEntity.builder().value("White").build());
        if (carRepository.existsByVinCodeOrGovNumber(vinCode1, govNumber1)) {
            return;
        } else {
            CarEntity carEntity = CarEntity.builder()
                    .govNumber(govNumber1)
                    .make("KIA")
                    .model("SPORTAGE")
                    .vinCode(vinCode1)
                    .year(LocalDate.of(2016,06, 21))
                    .ownerId(client.getId())
                    .owner(client)
                    .color(color1)
                    .build();
            carRepository.save(carEntity);
        }

        String govNumber2 = "AE5847HA";
        String vinCode2 = "JTHCE96S470011295";
        CarColorEntity color2 = carColorRepository.save(CarColorEntity.builder().value("Grey").build());
        if (carRepository.existsByVinCodeOrGovNumber(vinCode2, govNumber2)) {
            return;
        } else {
            CarEntity carEntity1 = CarEntity.builder()
                    .govNumber(govNumber2)
                    .make("Lexus")
                    .model("GS350")
                    .vinCode(vinCode2)
                    .year(LocalDate.of(2008,06, 21))
                    .ownerId(client.getId())
                    .owner(client)
                    .color(color2)
                    .build();
            carRepository.save(carEntity1);
        }
    }
}
