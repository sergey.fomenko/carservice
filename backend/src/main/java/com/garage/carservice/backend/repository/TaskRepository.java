package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.MaintenanceEntity;


public interface TaskRepository extends JpaRepository<MaintenanceEntity, Integer> {
}
