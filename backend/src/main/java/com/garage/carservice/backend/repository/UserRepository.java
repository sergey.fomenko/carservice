package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
}
