package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.customer.CarColorEntity;

public interface CarColorRepository extends JpaRepository<CarColorEntity, Integer> {
    boolean existsByValue(String value);
    CarColorEntity findByValue(String value);
}
