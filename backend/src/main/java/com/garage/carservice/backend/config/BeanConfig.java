//package com.garage.carservice.backend.config;
//
//import io.rsocket.core.RSocketServer;
//import io.rsocket.core.Resume;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.rsocket.server.RSocketServerCustomizer;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import reactor.util.retry.Retry;
//
//import java.time.Duration;
//
//@Slf4j
//@Configuration
//@EnableTransactionManagement
//public class BeanConfig implements RSocketServerCustomizer {
//
//    @Override
//    public void customize(RSocketServer rSocketServer) {
//        Resume resume =
//                new Resume()
//                        .sessionDuration(Duration.ofMinutes(15))
//                        .retry(Retry
//                                .fixedDelay(Long.MAX_VALUE, Duration.ofSeconds(5))
//                                .doBeforeRetry(s -> log.debug("Disconnected. Trying to resume...")));
//        rSocketServer.resume(resume);
//    }
//}
