package com.garage.carservice.backend.model.service;

import com.garage.carservice.backend.model.AuditableEntity;
import com.garage.carservice.backend.model.customer.CarEntity;
import com.garage.carservice.backend.model.warehouse.PartEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "maintenances")
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ServiceEntity extends AuditableEntity {

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "car_id")
    CarEntity car;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    List<TroubleshootingEntity> troubleshooting;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    List<PartEntity> parts;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    List<MaintenanceEntity> maintenance;
}
