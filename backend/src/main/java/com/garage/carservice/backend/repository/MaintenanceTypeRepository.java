package com.garage.carservice.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.garage.carservice.backend.model.service.MaintenanceTypeEntity;

public interface MaintenanceTypeRepository extends JpaRepository<MaintenanceTypeEntity, Integer> {
}
