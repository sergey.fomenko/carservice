package com.garage.carservice.backend.model.paging;

public enum Direction {

    asc,
    desc;
}
