import React, { PureComponent } from 'react';
import {Link} from "react-router-dom";

class NotFoundErrorPage extends PureComponent {

    render() {
        return (
            <div className="wrapper">
                <main>
                    <div className="container">
                        <div className="error-page error-abs">
                            <span className="icon icon-not_search" />
                            <h1>{'Похоже, что данной страницы не существует'}</h1>
                            <p>{'Попробуйте вернуться на главную.'}</p>
                            <p>
                                <Link to="/" replace
                                      className="btn btn-primary btn-big"
                                >
                                    <span className="icon-prev" />{'Вернуться на главную'}
                                </Link>
                            </p>
                        </div>
                    </div>
                </main>
                <div className="push" />
            </div>
        );
    }
}
export default NotFoundErrorPage
