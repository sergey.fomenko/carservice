import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { replace } from 'react-router-redux';
import {address} from "../data";
import {Client} from "../Client";


const mapStateToProps = state => ({

});

class LoginContainer extends PureComponent {


    handleInputChange = (e) => {
        const { name, value } = e.target;

        this.setState({
            [name]: value,
        });
    };

    handleSubmitForm = (e) => {
        e.preventDefault();
        const email = (this.state && this.state.email) || '';
        const password = (this.state && this.state.password) || '';
       /* this.props.onSubmit({ email, password });*/

        console.log('address.workPlace---> ', address.login)
        this.client = new Client('ws://localhost:7000');
        this.client.connect().then(sub => {
            this.setState({connected: true});
            console.log('handleSubmitForm')
            let msg = `${email}`;
            this.client.fireAndForget(msg, address.workPlaces).then(response => {
                console.log('response--> ', response)
                this.setState({
                    data: response
                })
            });
        });

    };

    render() {
        console.log('STATEE LOGIN--> ', this.state)
        const { errors } = this.props;
        return (
            <div className='container'>
                <div className="login-form">
                    <form onSubmit={this.handleSubmitForm}>
                        <h3 className="login-title">{'Login'}</h3>
                        <div className='form-group'>
                        <span >
                            <input
                                autoFocus
                                name="email"
                                type="text"
                                onChange={this.handleInputChange}
                                className="form-control e2eEmail"
                                placeholder={'Email'}/>
                        </span>
                            <span >
                            <input
                                name="password"
                                type="password"
                                onChange={this.handleInputChange}
                                className="form-control e2ePassword"
                                placeholder={'Пароль'}/>
                        </span>
                        </div>
                        <button type="submit" className="btn btn-primary ">{'ВОЙТИ'} <span className="icon-next"/></button>
                    </form>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, { replace })(LoginContainer)
