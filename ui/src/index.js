import React from 'react';
import ReactDOM from 'react-dom';
import './style/custom.scss';

import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import {
    BrowserRouter as Router} from "react-router-dom";
import configureStore from './store';

import Routes from "./components/Routes";


let history = createBrowserHistory();

const store = configureStore(createBrowserHistory)

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Routes />
        </Router>
    </Provider>,
  document.getElementById('root')
);


