import React, {Component} from 'react';
import {MDBDataTable} from "mdbreact";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

export default class Main extends Component{

    constructor(props) {

        super(props);
        this.state= {
            posts: [],
            isLoading:true,
            tableRows: [],
        };
    }

    componentWillMount=async() => {
        let response = await fetch('https://jsonplaceholder.typicode.com/comments');
        let json = await response.json();
        this.setState({ posts: json }, () => {
            this.setState({ tableRows:this.assemblePosts(), isLoading:false })
            console.log(this.state.tableRows);
        })
    }

    assemblePosts= () => {
        let posts =this.state.posts && this.state.posts.map((post) => {
            return (
                {
                    number: post.id,
                    title: post.id,
                    user: post.name,
                    body: post.body,
                }
            )
        });
        return posts;
    }

    render() {
        console.log('erwe--> ', this.state)
        const data = {
            columns: [
                {
                    label:'#',
                    field:'number',
                },
                {
                    label:'Title',
                    field:'title',
                },
                {
                    label:'User ID',
                    field:'user',
                },
                {
                    label:'Body',
                    field:'body',
                },

            ],
            rows:this.state.tableRows,
        }
        return (
            <div className='container'>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    data={data}
                />
            </div>

    )}
    }

