import React from "react";
import {Route, IndexRoute, Redirect, Switch} from "react-router";

import {PATH} from "../data/index";
import App from "../App";
import LoginContainer from "../container/LoginContainer";
import Main from "./Main";
import NotFoundErrorPage from "../container/error/NotFoundErrorPage";
import {Link} from "react-router-dom";
import Elevator from "./Elevator";

export const DEFAULT_PATH = `/${PATH.MAIN.INDEX}`;
const FORBIDDEN_PAGE_REDIRECT_PATH = `/${PATH.FORBIDDEN}`;

const Routes = (store) => (
    <div>
    <header>
       <div className='container'>
           <div className='header-wrap'>
               <Link to="/">
                   <h1 className='logo'>CarService</h1>
               </Link>
               <nav>
                   <ul>
                       <li>
                           <Link to="/">Home</Link>
                       </li>
                       <li>
                           <Link to="/elevator">elevator</Link>
                       </li>
                       <li>
                           <Link to="/main">Main</Link>
                       </li>
                       <li>
                           <Link to="/login">Login</Link>
                       </li>
                   </ul>
               </nav>
           </div>
           </div>
    </header>
        <Switch>
            <Route exact path={`/`} component={App}/>
            <Route exact path={`/login`} component={LoginContainer}/>
            <Route exact path="/main" component={Main}/>
            <Route exact path="/elevator" component={Elevator}/>
            <Route path="*" component={NotFoundErrorPage}/>
        </Switch>
    </div>
);

export default Routes;