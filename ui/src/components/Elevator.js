import React, { PureComponent } from 'react';
import {Client} from "../Client";
import { address } from "../data";

class Elevator extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            connected: false,
            streamInProgress: false,
            channelInProgress: false,
            data: []
        }

    }
    componentDidMount() {
        console.log('address.workPlace---> ', address.workPlaces)
        this.client = new Client('ws://localhost:7000');
        this.client.connect().then(sub => {
            this.setState({connected: true});
            console.log('componentDidMount')
            let msg = 'string';
            this.client.requestResponse(msg, address.workPlaces).then(response => {
                console.log('response--> ', response)
                this.setState({
                    data: response
                })
            });
        });
    }

    render() {
        const { errors } = this.props;
        console.log('STATEWE--> ', this.state)
        return (
            <div className='custom-bg'>
                <div className='container'>
                    <h3 className="login-title">{'Elevator'}</h3>

                    <section className='elevators-list'>
                        {this.state.data && this.state.data.map((item, indx) => {
                            return (
                                <article key={indx} className='col-sm-3'>
                                    <h2>{item.value}</h2>
                                    <div className='elevator-wrap'>
                                        {item.jobs && item.jobs.map((job, indx) => {
                                            return(
                                                <div key={indx}>
                                                    <h3>{job.car && job.car.map((car) => {
                                                        return(
                                                            <div key={car.id}>
                                                                <h3>{car.value}</h3>
                                                                <img src={car.carLogo} alt={car.value} />
                                                            </div>
                                                        )})
                                                    }</h3>
                                                    <div>
                                                        <span>{job.startTime}</span>
                                                        <div>{job.value}</div>
                                                        <div>{job.durationTime}</div>
                                                        <img src={job.workerPhoto} alt={job.value} />
                                                    </div>
                                                </div>
                                            )})
                                        }
                                    </div>
                                </article>
                            )
                        })}
                    </section>
                </div>
            </div>
        );
    }
}
export default Elevator
