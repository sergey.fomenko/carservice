import { fromJS } from 'immutable';
import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import rootReducer from '../reducers';
import rootSaga from '../sagas';
import { createLogger } from 'redux-logger';

const logger = (process.env.NODE_ENV === 'development') ? createLogger({
    collapsed: true,
    duration: true,
    diff: true,
    stateTransformer: state => state.toJS(),
}) : (() => noop => noop);

export default function configureStore(history) {
    const sagaMiddleware = createSagaMiddleware();

    const middlewares = [
        sagaMiddleware,
        routerMiddleware(history),
    ];

    const enhancers = [
        composeWithDevTools(applyMiddleware(...middlewares, logger)),
    ];

    const store = createStore(
        rootReducer,
        fromJS({}),
        compose(...enhancers),
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
