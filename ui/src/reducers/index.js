import { combineReducers } from 'redux-immutable';

import routing from './routing';

export default combineReducers({
    routing
});
