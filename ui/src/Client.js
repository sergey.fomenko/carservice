import {
    IdentitySerializer,
    JsonSerializer,
    APPLICATION_JSON,
    MESSAGE_RSOCKET_ROUTING,
    RSocketClient,
    RSocketResumableTransport} from "rsocket-core";

import RSocketWebSocketClient from "rsocket-websocket-client";
import {v4 as uuidv4} from 'uuid';
import {BufferEncoders, encodeCompositeMetadata, encodeRoute, encodeSimpleAuthMetadata, MESSAGE_RSOCKET_AUTHENTICATION, TEXT_PLAIN} from "rsocket-core/build";

export class Client {

    constructor(address) {
        this.client = new RSocketClient({
            serializers: {
                data: JsonSerializer,
                metadata: IdentitySerializer
            },
            setup: {
                keepAlive: 10000,
                lifetime: 20000,
                dataMimeType: APPLICATION_JSON.string,
                metadataMimeType: MESSAGE_RSOCKET_ROUTING.string,
                payload: {
                    metadata: encodeCompositeMetadata([
                        [MESSAGE_RSOCKET_AUTHENTICATION, encodeSimpleAuthMetadata('user', 'user')]
                    ])
                },
            },
            // transport: new RSocketResumableTransport(
            //     () => new RSocketWebSocketClient({url: address}),
            //     {
            //         bufferSize: 100,
            //         resumeToken: uuidv4(),
            //     })
            transport: new RSocketWebSocketClient({
                url: "ws://localhost:7000",
            }, BufferEncoders),
        });
    }

    requestResponse(message, address) {
        return new Promise((resolve, reject) => {
            this.socket.requestResponse({
                data: message,
                metadata: String.fromCharCode(address.length) + address
            }).subscribe({
                onComplete: msg => {
                    resolve(msg.data)
                },
                onError: error => {
                    reject(error)
                }
            });
        });
    }

    fireAndForget(message) {
        return this.socket.fireAndForget({
            data: message,
            metadata: encodeCompositeMetadata([
                [TEXT_PLAIN, Buffer.from('Hello World')],
                [MESSAGE_RSOCKET_ROUTING, encodeRoute("fire-and-forget")],
                [MESSAGE_RSOCKET_AUTHENTICATION, encodeSimpleAuthMetadata("user", "user")],
            ])
        });
    }

    requestStream(message) {
        return this.socket.requestStream({
            data: message,
            metadata: String.fromCharCode('stream'.length) + 'stream'
        });
    }

    requestChannel(flow) {
        return this.socket.requestChannel(flow.map(msg => {
            return {
                data: msg,
                metadata: String.fromCharCode('channel'.length) + 'channel'
            };
        }));
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.client.connect().subscribe({
                onComplete: s => {
                    this.socket = s;
                    this.socket.connectionStatus().subscribe(status => {
                        console.log(status);
                    });

                    resolve(this.socket);
                },
                onError: error => {
                    reject(error);
                },
                onSubscribe: cancel => {
                    this.cancel = cancel
                }
            });
        });
    }

    disconnect() {
        console.log('rsocketclientsocket', this.socket);
        console.log('rsocketclient', this.client);
        this.client.close();
    }

}
