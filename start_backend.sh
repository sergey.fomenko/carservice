#!/usr/bin/env bash

BUILD_DIR="$(cd "$(dirname "$0")" && pwd)"
cd auth
mvn spring-boot:run
cd backend
mvn spring-boot:run -Dspring-boot.run.profiles=h2