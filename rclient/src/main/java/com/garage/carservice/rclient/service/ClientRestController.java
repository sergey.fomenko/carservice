package com.garage.carservice.rclient.service;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * @author feuyeux@gmail.com
 */
@Slf4j
@RestController
@RequestMapping("api")
public class ClientRestController {
    @Autowired
    private RSocketAdapter helloRSocketAdapter;

    @GetMapping("signin")
    Mono<HelloToken> signIn(@RequestParam String u, @RequestParam String p) {
        log.info("[signIn] input={},{}", u, p);
        return helloRSocketAdapter.signIn(u, p);
    }

    @GetMapping("refresh/{token}")
    Mono<HelloToken> refresh(@PathVariable String token) {
        log.info("[refresh] input={}", token);
        return helloRSocketAdapter.refresh(token);
    }

    @PostMapping(value = "signout")
    Mono<Void> signOut() {
        log.info("[signOut]");
        return helloRSocketAdapter.signOut();
    }

    @PostMapping(value = "hire", consumes = APPLICATION_STREAM_JSON_VALUE)
    Mono<HelloResponse> hire(@RequestBody Mono<HelloRequest> requestStream) {
        log.info("[hire]");
        return helloRSocketAdapter.hire(requestStream);
    }

    @PostMapping(value = "fire", consumes = APPLICATION_STREAM_JSON_VALUE)
    Mono<HelloResponse> fire(@RequestBody Mono<HelloRequest> requestStream) {
        log.info("[fire]");
        return helloRSocketAdapter.fire(requestStream);
    }

    @GetMapping("info/{id}")
    Mono<HelloResponse> info(@PathVariable long id) {
        log.info("[info] input={}", id);
        return helloRSocketAdapter.info(id);
    }

    @GetMapping(value = "list", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Publisher<HelloResponse> list() {
        log.info("[list]");
        return helloRSocketAdapter.list();
    }
}
