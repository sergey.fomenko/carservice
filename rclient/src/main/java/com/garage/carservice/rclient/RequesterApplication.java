package com.garage.carservice.rclient.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequesterApplication {

    public static void main(String... args) {
        SpringApplication.run(RequesterApplication.class, args);
    }

}
